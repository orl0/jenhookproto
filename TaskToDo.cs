﻿public class TaskToDo
{
    public string Title { get; set; }
    public bool IsDone { get; set; }
}